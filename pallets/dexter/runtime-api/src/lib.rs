
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! Runtime API definition for dexter module.

#![cfg_attr(not(feature = "std"), no_std)]

pub use pallet_dexter::{Order, OrderStatus, ClosedDeal};

pub use frame_support::sp_std::vec::Vec;

sp_api::decl_runtime_apis! {
	pub trait DexterApi
	{
		fn get_order_status(order: Order) -> OrderStatus;

		fn get_bid_orders(
			limit: u32,
		) -> Vec<Order>;

		fn get_ask_orders(
			limit: u32,
		) -> Vec<Order>;

		fn get_closed_orders(
			limit: u32,
		) -> Vec<ClosedDeal>;
	}
}
