
use codec::{Encode, Decode};

#[cfg(feature = "std")]
use frame_support::serde::{Serialize, Deserialize};

pub type ClientId = u64;
pub type OrderId = u32;
pub type Balance = u32;

#[derive(Copy, Eq, PartialEq, Encode, Decode, Clone, Debug)]
#[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "std", serde(rename_all = "camelCase"))]
pub enum Coin {
    Dot, 
    Inno
}

#[derive(Eq, PartialEq, Encode, Decode, Clone, Debug)]
#[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "std", serde(rename_all = "camelCase"))]
pub enum Order {
    IdleOrder,
    LimitOrder {
        id: OrderId,
        owner: ClientId,
        token: Coin,
        threshold: Balance,
    },
    MarketOrder {
        id: OrderId,
        owner: ClientId,
        token: Coin,
    }
}

impl Order {
    pub fn id(&self) -> OrderId {
        match &self {
            Order::IdleOrder => 0,
            Order::LimitOrder {id, ..} => *id,
            Order::MarketOrder {id, ..} => *id,
        }
    }

    pub fn owner(&self) -> ClientId {
        match &self {
            Order::IdleOrder => 0,
            Order::LimitOrder {owner, ..} => *owner,
            Order::MarketOrder {owner, ..} => *owner,
        }
    }

    pub fn coin(&self) -> Coin {
        match &self {
            Order::IdleOrder => Coin::Dot,
            Order::LimitOrder {token, ..} => *token,
            Order::MarketOrder {token, ..} => *token,
        }
    }

    pub fn limit(&self) -> Option<Balance> {
        match &self {
            Order::IdleOrder => None,
            Order::LimitOrder {threshold, ..} => Some(*threshold),
            Order::MarketOrder {..} => None,
        }
    }
}

impl Default for Order {
    fn default() -> Order {
        Order::IdleOrder
    }
}

#[derive(Eq, PartialEq, Encode, Decode, Clone, Debug)]
#[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "std", serde(rename_all = "camelCase"))]
pub enum OrderParams {
    IdleOrder,
    LimitOrder(Balance, Coin),
    MarketOrder(Coin),
}

impl Order {
    pub fn create(id: OrderId, owner: ClientId, params: OrderParams) -> Order {
        match params {
            OrderParams::IdleOrder => Order::IdleOrder,
            OrderParams::LimitOrder (limit, coin) => Order::LimitOrder { id: id, owner: owner, token: coin, threshold: limit},
            OrderParams::MarketOrder (coin) => Order::MarketOrder { id: id, owner: owner, token: coin},
        }
    }
}

#[derive(Eq, PartialEq, Encode, Decode, Default)]
#[cfg_attr(feature = "std", derive(Debug, Serialize, Deserialize))]
#[cfg_attr(feature = "std", serde(rename_all = "camelCase"))]
pub struct OrderStatus {
    pub successful: bool,
}

impl OrderStatus {
    pub fn create_successful() -> OrderStatus {
        OrderStatus { successful: true }
    }
}