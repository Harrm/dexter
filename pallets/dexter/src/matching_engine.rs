use frame_support::sp_std::vec::Vec;
use codec::{Encode, Decode};
use super::order::{Order,OrderId, Coin, ClientId};
use super::order_book::{OrderBook};

#[cfg(feature = "std")]
use frame_support::serde::{Serialize, Deserialize};

#[derive(Eq, PartialEq, Encode, Decode, Clone, Debug)]
#[cfg_attr(feature = "std", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "std", serde(rename_all = "camelCase"))]
pub struct ClosedDeal {
    pub ask: OrderId,
    pub bid: OrderId,
}

pub struct MatchingEngineImpl {

}

pub trait MatchingEngine {

    fn match_ask_order(&self, order: Order, order_book: &OrderBook) -> Option<ClosedDeal>;
    fn match_bid_order(&self, order: Order, order_book: &OrderBook) -> Option<ClosedDeal>;

} 

impl MatchingEngine for MatchingEngineImpl {

    fn match_ask_order(&self, order: Order, order_book: &OrderBook) -> Option<ClosedDeal> {
        match order {
            Order::LimitOrder{id, owner, token, threshold} => {
                let best_bid = order_book.get_bid_with_limit(owner, token, threshold)?;
                Some(ClosedDeal{ask: id, bid: best_bid})
            },
            Order::MarketOrder{id, owner, token} => {
                let best_bid = order_book.get_best_bid(owner, token)?;
                Some(ClosedDeal{ask: id, bid: best_bid})
            },
            _ => None
        }
    }
    fn match_bid_order(&self, order: Order, order_book: &OrderBook) -> Option<ClosedDeal> {
        match order {
            Order::LimitOrder{id, owner, token, threshold} => {
                let best_ask = order_book.get_ask_with_limit(owner, token, threshold)?;
                Some(ClosedDeal{ask: best_ask, bid: id})
            },
            Order::MarketOrder{id, owner, token} => {
                let best_ask = order_book.get_best_ask(owner, token)?;
                Some(ClosedDeal{ask: best_ask, bid: id})
            },
            _ => None
        }
    }

}