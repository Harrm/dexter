#![cfg_attr(not(feature = "std"), no_std)]

/// Edit this file to define custom logic or remove it if it is not needed.
/// Learn more about FRAME and the core library of Substrate FRAME pallets:
/// https://substrate.dev/docs/en/knowledgebase/runtime/frame
use frame_support::{
	decl_error, decl_event, decl_module, decl_storage, dispatch, traits::Get,
	weights::DispatchInfo,
};
use frame_system::ensure_signed;
use sp_std::prelude::*;
use sp_runtime::{traits::Dispatchable, FixedPointOperand};

#[cfg(test)]
mod mock;

#[cfg(test)]
mod tests;

mod order;
mod order_book;
mod matching_engine;

pub use order::{ClientId, Balance, OrderId, Coin, Order, OrderStatus, OrderParams};
pub use matching_engine::{MatchingEngine, MatchingEngineImpl, ClosedDeal};
pub use order_book::{OrderBook, StorageOrderBook};

/// Configure the pallet by specifying the parameters and types on which it depends.
pub trait Trait: frame_system::Trait {
	/// Because this pallet emits events, it depends on the runtime's definition of an event.
	type Event: From<Event<Self>> + Into<<Self as frame_system::Trait>::Event>;
}

// The pallet's runtime storage items.
// https://substrate.dev/docs/en/knowledgebase/runtime/storage
decl_storage! {
	// A unique name is used to ensure that the pallet's storage items are isolated.
	// This name may be updated, but each pallet in the runtime must use a unique name.
	// ---------------------------------vvvvvvvvvvvvvv
	trait Store for Module<T: Trait> as TemplateModule {
		NextOrderId get(fn get_next_order_id): OrderId = 1;
		NextClientId get(fn get_next_client_id): ClientId = 1;
		pub OpenAskOrders get(fn get_ask_order): map hasher(identity) OrderId => Order;
		pub OpenBidOrders get(fn get_bid_order): map hasher(identity) OrderId => Order;
		pub ClosedOrders get(fn get_closed_order): map hasher(identity) OrderId => Order;
		pub InnoToken get(fn get_inno_balance): map hasher(blake2_128_concat) ClientId => Balance = 100;
		pub DotToken get(fn get_dot_balance): map hasher(blake2_128_concat) ClientId => Balance = 100;
		pub AccountClientId get(fn get_client_id): map hasher(blake2_128_concat) T::AccountId => ClientId;
	}
}

// Pallets use events to inform users when important changes are made.
// https://substrate.dev/docs/en/knowledgebase/runtime/events
decl_event!(
	pub enum Event<T>
	where
		AccountId = <T as frame_system::Trait>::AccountId,
	{
		// [_]
		PhantomEvent(AccountId),
		// [order]
		OrderOpen(Order),
		// [ask_order, bid_order]
		DealClosed(OrderId, OrderId),
		// [order]
		OrderCancelled(OrderId),
	}
);

// Errors inform users that something went wrong.
decl_error! {
	pub enum Error for Module<T: Trait> {
		/// Error names should be descriptive.
		NoneValue,
		/// Errors should have helpful documentation associated with them.
		StorageOverflow,
		// Owner id mismatch
		AccessDenied,
		// Order with given id not found
		OrderNotFound,
		// balance is too low to complete the operation
		LowBalance, 
		// Market order couldn't resolve immediately 
		StrayMarketOrder,
	}
}

// Dispatchable functions allows users to interact with the pallet and invoke state changes.
// These functions materialize as "extrinsics", which are often compared to transactions.
// Dispatchable functions must be annotated with a weight and must return a DispatchResult.
decl_module! {
	pub struct Module<T: Trait> for enum Call where origin: T::Origin {
		// Errors must be initialized if they are used by the pallet.
		type Error = Error<T>;

		// Events must be initialized if they are used by the pallet.
		fn deposit_event() = default;

		#[weight = 10_000 + T::DbWeight::get().reads_writes(1,1)]
		pub fn place_ask_order(origin, params: OrderParams) -> dispatch::DispatchResult {
			let who = ensure_signed(origin)?;
			
			let client_id = Module::<T>::get_or_create_client_id(&who);

			let order = Module::<T>::create_order(client_id, params);

			let order_book = StorageOrderBook::create(
				OpenAskOrders::iter().map(|(_, ord)| ord.clone()), 
				OpenBidOrders::iter().map(|(_, ord)| ord.clone()));
			let matching_engine = MatchingEngineImpl {};

			if let Some(ClosedDeal {ask, bid}) = matching_engine.match_ask_order(order.clone(), &order_book) {
				let bid_order = OpenBidOrders::get(bid);
				if Module::<T>::transfer(&order, &bid_order) {
					ClosedOrders::insert(ask, order.clone());
					ClosedOrders::insert(bid, bid_order);
					OpenBidOrders::remove(bid);
					Self::deposit_event(Event::<T>::DealClosed(ask, bid));
				} else {
					Module::<T>::lock_bid_limit(bid_order.owner(), bid_order.coin());
					Err(Error::<T>::LowBalance)?;
				}
			} else {
				if order.limit().is_none() {
					Err(Error::<T>::StrayMarketOrder)?;
				}
				// will be unlocked once the order is resolverd
				Module::<T>::lock_ask_limit(client_id, order.limit().unwrap(), order.coin());
				OpenAskOrders::insert(order.id(), order.clone());	
				Self::deposit_event(Event::<T>::OrderOpen(order));
			}
			Ok(())
		}

		#[weight = 10_000 + T::DbWeight::get().reads_writes(1,1)]
		pub fn place_bid_order(origin, params: OrderParams) -> dispatch::DispatchResult {
			let who = ensure_signed(origin)?;
			let client_id = Module::<T>::get_or_create_client_id(&who);

			let order_book = StorageOrderBook::create(
				OpenAskOrders::iter().map(|(_, ord)| ord.clone()), 
				OpenBidOrders::iter().map(|(_, ord)| ord.clone()));
			let matching_engine = MatchingEngineImpl {};
			let order = Module::<T>::create_order(client_id, params);
			if let Some(ClosedDeal {ask, bid}) = matching_engine.match_bid_order(order.clone(), &order_book) {
				let ask_order = OpenAskOrders::get(ask);
				if Module::<T>::transfer(&ask_order, &order) {
					ClosedOrders::insert(ask, ask_order);
					ClosedOrders::insert(bid, order.clone());
					OpenAskOrders::remove(ask);
					Self::deposit_event(Event::<T>::DealClosed(ask, bid));
				} else {
					Module::<T>::lock_ask_limit(ask_order.owner(), ask_order.limit().unwrap(), ask_order.coin());
					Err(Error::<T>::LowBalance)?;
				}
			} else {
				Module::<T>::lock_bid_limit(client_id, order.coin());
				OpenBidOrders::insert(order.id(), order.clone());	
				Self::deposit_event(Event::<T>::OrderOpen(order));
			}
			Ok(())
		}

		#[weight = 10_000 + T::DbWeight::get().reads_writes(1,1)]
		pub fn cancel_order(origin, id: OrderId) -> dispatch::DispatchResult {
			let who = ensure_signed(origin)?;
			let client_id: ClientId = Self::get_client_id(who);
			if OpenAskOrders::contains_key(id) {
				let order = OpenAskOrders::get(id);
				if order.owner() != client_id {
					Err(Error::<T>::AccessDenied)?;
				}
				OpenAskOrders::remove(id);
			} else if OpenBidOrders::contains_key(id) {
				let order = OpenBidOrders::get(id);
				if order.owner() != client_id {
					Err(Error::<T>::AccessDenied)?;
				}
				OpenBidOrders::remove(id);
			}
			Self::deposit_event(Event::<T>::OrderCancelled(id));
			Err(Error::<T>::OrderNotFound)?	
		}
	}
}

impl<T: Trait> Module<T> {
	
	pub fn get_order_status(_order: Order) -> OrderStatus
	where
		T: Send + Sync,
		T::Call: Dispatchable<Info = DispatchInfo>,
	{
		OrderStatus::create_successful()
	}

	pub fn get_bid_orders(
		limit: u32,
	) -> Vec<Order> {
		OpenBidOrders::iter().map(|(_, ord)| ord.clone()).take(limit as usize).collect::<Vec<Order>>()
	}

	pub fn get_ask_orders(
		limit: u32,
	) -> Vec<Order> {
		OpenAskOrders::iter().map(|(_, ord)| ord.clone()).take(limit as usize).collect::<Vec<Order>>()
	}

	pub fn get_closed_orders(
		limit: u32,
	) -> Vec<ClosedDeal> {
		let order_ids = ClosedOrders::iter().map(|(ord_id, _)| ord_id).collect::<Vec<OrderId>>();
		let ask_chunks = order_ids.clone().into_iter().step_by(2);
		let bid_chunks = order_ids.clone().into_iter().skip(1).step_by(2);
		ask_chunks.zip(bid_chunks).map(|(ask, bid)| ClosedDeal{ask, bid}).take(limit as usize).collect::<Vec<ClosedDeal>>()
	}

	fn get_or_create_client_id(who: &T::AccountId) -> ClientId {
		if !AccountClientId::<T>::contains_key(&who) {
			let id =  NextClientId::get();
			AccountClientId::<T>::insert(&who, id);
			NextClientId::put(id + 1);
			// for testing purposes
			InnoToken::insert(id, 100);
			DotToken::insert(id, 100);
			return id;
		}
		Self::get_client_id(who)
	}

	fn create_order(client_id: ClientId, params: OrderParams) -> Order {
		let next_order_id = NextOrderId::get();
		NextOrderId::put(next_order_id + 1);
		Order::create(next_order_id, client_id, params)
	}

	fn lock_bid_limit(owner: ClientId, coin: Coin) -> bool {
		match coin {
			Coin::Inno => {
				let balance = InnoToken::get(owner);
				if balance >= 100 {
					InnoToken::insert(owner, balance - 100);
					true
				} else {
					false
				}	
			}, 
			Coin::Dot => {
				let balance = DotToken::get(owner);
				if balance >= 100 {
					DotToken::insert(owner, balance - 100);
					true
				} else {
					false
				}	
			}
		}
	}

	fn lock_ask_limit(owner: ClientId, threshold: Balance, coin: Coin) -> bool {
		match coin {
			Coin::Inno => {
				let balance = DotToken::get(owner);
				if balance >= threshold {
					DotToken::insert(owner, balance - threshold);
					true
				} else {
					false
				}	
			}, 
			Coin::Dot => {
				let balance = InnoToken::get(owner);
				if balance >= threshold {
					InnoToken::insert(owner, balance - threshold);
					true
				} else {
					false
				}	
			}
		}
	}

	fn complete_transfer(ask: &Order, bid: &Order) -> bool {
		assert_eq!(ask.coin(), bid.coin());
		let (bid_id, bid_owner, bid_limit) = (bid.id(), bid.owner(), bid.limit());
		let (ask_id, ask_owner, ask_limit) = (ask.id(), ask.owner(), ask.limit());
		let deal_cost = if bid_limit == None && ask_limit != None {
			ask_limit.unwrap()

		} else if bid_limit != None && ask_limit == None {
			bid_limit.unwrap()

		} else if bid_limit != None && ask_limit != None {
			// bid 10, ask 20 => sell at least for 10, buy at most for 20
			// if buyer chooses, he chooses to buy for 10
			if ask_id > bid_id {
				bid_limit.unwrap()
			} else {
				ask_limit.unwrap()
			}
		} else {
			// market price
			panic!("Not implemented!");
		};
		match ask.coin() {
			Coin::Inno => {
				let dot_cost = deal_cost;
				let ask_dot_balance = DotToken::get(ask_owner);
				let bid_inno_balance = InnoToken::get(bid_owner);
				if ask_dot_balance >= dot_cost && bid_inno_balance >= 100 {
					DotToken::insert(ask_owner, ask_dot_balance - dot_cost);
					DotToken::mutate(bid_owner, |balance| *balance += dot_cost);
					InnoToken::insert(bid_owner, bid_inno_balance - 100);
					InnoToken::mutate(ask_owner, |balance| *balance += 100);
					true
				} else {
					false
				}
			},
			Coin::Dot => {
				let inno_cost = deal_cost;
				let ask_inno_balance = InnoToken::get(ask_owner);
				let bid_dot_balance = DotToken::get(bid_owner);
				if ask_inno_balance >= inno_cost && bid_dot_balance >= 100 {
					InnoToken::insert(ask_owner, ask_inno_balance - inno_cost);
					InnoToken::mutate(bid_owner, |balance| *balance += inno_cost);
					DotToken::insert(bid_owner, bid_dot_balance - 100);
					DotToken::mutate(ask_owner, |balance| *balance += 100);
					true
				} else {
					false
				}
			}
		}
	}

	fn transfer(ask: &Order, bid: &Order) -> bool {
		assert_eq!(ask.coin(), bid.coin());
		// unlock the deferred balance
		if ask.id() < bid.id() {
			match ask.coin() {
				Coin::Inno => {
					if let Some(limit) = ask.limit() {
						DotToken::mutate(ask.owner(), |balance| *balance += limit);
					}					
				},
				Coin::Dot => {
					if let Some(limit) = ask.limit() {
						InnoToken::mutate(ask.owner(), |balance| *balance += limit);
					}					
				}
			}	
		} else {
			match bid.coin() {
				Coin::Inno => {
					InnoToken::mutate(bid.owner(), |balance| *balance += 100);
				}, 
				Coin::Dot => {
					DotToken::mutate(bid.owner(), |balance| *balance += 100);					
				}
			}	
		}
		Self::complete_transfer(ask, bid)
	}
}

