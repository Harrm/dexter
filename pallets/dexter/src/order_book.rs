use frame_support::sp_std::{vec::Vec};
use super::order::{Order, OrderId, Coin, Balance, ClientId};

pub trait OrderBook {
    fn get_best_ask(&self, for_client: ClientId, token: Coin) -> Option<OrderId>;
    fn get_best_bid(&self, for_client: ClientId, token: Coin) -> Option<OrderId>;
    fn get_ask_with_limit(&self, for_client: ClientId, token: Coin, limit: Balance) -> Option<OrderId>;
    fn get_bid_with_limit(&self, for_client: ClientId, token: Coin, limit: Balance) -> Option<OrderId>;
}

pub struct StorageOrderBook {
    inno_ask_orders: Vec<Order>,
    inno_bid_orders: Vec<Order>,
    dot_ask_orders: Vec<Order>,
    dot_bid_orders: Vec<Order>,
}

impl StorageOrderBook {
    pub fn create<AskIt, BidIt>(ask_orders_it: AskIt, bid_orders_it: BidIt) -> StorageOrderBook
        where AskIt: Iterator<Item = Order>,
              BidIt: Iterator<Item = Order> {
        let mut ask_orders = ask_orders_it.filter(|ord| *ord != Order::IdleOrder).collect::<Vec<Order>>();
        ask_orders.sort_by(
            |order1, order2| {
                let order1_priority = Self::get_ask_order_priority(order1);
                let order2_priority = Self::get_ask_order_priority(order2);
                return order1_priority.partial_cmp(&order2_priority).unwrap();
            }
        );
        let mut bid_orders = bid_orders_it.filter(|ord| *ord != Order::IdleOrder).collect::<Vec<Order>>();
        bid_orders.sort_by(
            |order1, order2| {
                let order1_priority = Self::get_bid_order_priority(order1);
                let order2_priority = Self::get_bid_order_priority(order2);
                return order1_priority.partial_cmp(&order2_priority).unwrap();
            }
        );
        let inno_ask_orders: Vec<Order> = ask_orders.iter().filter(|ord| ord.coin() == Coin::Inno).map(|ord| ord.clone()).collect();
        let dot_ask_orders: Vec<Order> = ask_orders.iter().filter(|ord| ord.coin() == Coin::Dot).map(|ord| ord.clone()).collect();
        let inno_bid_orders: Vec<Order> = bid_orders.iter().filter(|ord| ord.coin() == Coin::Inno).map(|ord| ord.clone()).collect();
        let dot_bid_orders: Vec<Order> = bid_orders.iter().filter(|ord| ord.coin() == Coin::Dot).map(|ord| ord.clone()).collect();
        StorageOrderBook { 
            inno_ask_orders,
            inno_bid_orders,
            dot_ask_orders,
            dot_bid_orders, } 
    }

    fn get_ask_order_priority(order: &Order) -> u32 {
        match order {
            Order::IdleOrder => Balance::MAX,
            Order::LimitOrder {threshold, ..} => Balance::MAX - *threshold,
            Order::MarketOrder {..} => Balance::MAX,
        }
    }

    fn get_bid_order_priority(order: &Order) -> u32 {
        match order {
            Order::IdleOrder => Balance::MAX,
            Order::LimitOrder {threshold, ..} => *threshold,
            Order::MarketOrder {..} => Balance::MAX,
        }
    }

    fn get_ask_orders_for(&self, coin: Coin) -> &Vec<Order> {
        match coin {
            Coin::Dot => &self.dot_ask_orders,
            Coin::Inno => &self.inno_ask_orders,
        }
    }
    
    fn get_bid_orders_for(&self, coin: Coin) -> &Vec<Order> {
        match coin {
            Coin::Dot => &self.dot_bid_orders,
            Coin::Inno => &self.inno_bid_orders,
        }
    }
}

impl OrderBook for StorageOrderBook {
    fn get_best_ask(&self, for_client: ClientId, token: Coin) -> Option<OrderId> {
        let ask_orders = self.get_ask_orders_for(token);
        ask_orders.iter().find(|ord| ord.owner() != for_client).map(|v| v.id())
    }

    fn get_best_bid(&self, for_client: ClientId, token: Coin) -> Option<OrderId> {
        let bid_orders = self.get_bid_orders_for(token);
        bid_orders.iter().find(|ord| ord.owner() != for_client).map(|v| v.id())
    }

    // bid innocoin limit 10 -> sell 1 innocoin for at least 10 dots (better more)
    // look for ask with limit 10 or more (better more)
    fn get_ask_with_limit(&self, for_client: ClientId, coin: Coin, limit: Balance) -> Option<OrderId> {
        let ask_orders = self.get_ask_orders_for(coin);
        ask_orders.iter().find(|ord| ord.owner() != for_client && match ord {
            Order::LimitOrder {threshold, ..} => *threshold >= limit,
            Order::MarketOrder {..} => false,
            Order::IdleOrder => false
        }).map(|v| v.id())
    }

    // ask innocoin limit 10 -> buy 1 innocoin for no more than 10 dots (better less)
    // look for bid with limit 10 or less (better less)
    fn get_bid_with_limit(&self, for_client: ClientId, coin: Coin, limit: Balance) -> Option<OrderId> {
        let bid_orders = self.get_bid_orders_for(coin);
        bid_orders.iter().find(|ord| ord.owner() != for_client && match ord {
            Order::LimitOrder {threshold, ..} => *threshold <= limit,
            Order::MarketOrder {..} => false,
            Order::IdleOrder => false
        }).map(|v| v.id())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_empty() {
        let bids: Vec<Order> = vec![];
        let asks: Vec<Order> = vec![];
        let book = StorageOrderBook::create(asks.into_iter(), bids.into_iter());
        assert_eq!(book.get_best_ask(0, Coin::Inno), None);
        assert_eq!(book.get_best_bid(0, Coin::Inno), None);
        assert_eq!(book.get_bid_with_limit(0, Coin::Inno, 0), None);
        assert_eq!(book.get_ask_with_limit(0, Coin::Inno, 0), None);
    }

    #[test]
    fn test_scenario_1() {
        let bids: Vec<Order> = vec![
            Order::LimitOrder { id: 0, owner: 0, token: Coin::Inno, threshold: 2 },
            Order::MarketOrder { id: 1, owner: 1, token: Coin::Inno },
            Order::LimitOrder { id: 2, owner: 2, token: Coin::Inno, threshold: 1 },
            Order::MarketOrder { id: 3, owner: 3, token: Coin::Inno },
            Order::LimitOrder { id: 4, owner: 4, token: Coin::Inno, threshold: 3 },
        ];
        let asks: Vec<Order> = vec![
            Order::LimitOrder { id: 5, owner: 4, token: Coin::Inno, threshold: 3 },
            Order::MarketOrder { id: 6, owner: 5, token: Coin::Inno },
            Order::LimitOrder { id: 7, owner: 7, token: Coin::Inno, threshold: 1 },
            Order::MarketOrder { id: 8, owner: 8, token: Coin::Inno },
            Order::LimitOrder { id: 9, owner: 9, token: Coin::Inno, threshold: 2 },
        ];
        let book = StorageOrderBook::create(asks.into_iter(), bids.into_iter());
        assert_eq!(book.get_best_ask(10, Coin::Dot), None);
        assert_eq!(book.get_best_bid(10, Coin::Dot), None);
        assert_eq!(book.get_bid_with_limit(10, Coin::Dot, 0), None);
        assert_eq!(book.get_ask_with_limit(10, Coin::Dot, 0), None);

        assert_eq!(book.get_best_ask(10, Coin::Inno), Some(5));
        assert_eq!(book.get_best_bid(10, Coin::Inno), Some(2));

        assert_eq!(book.get_bid_with_limit(10, Coin::Inno, 0), None);
        assert_eq!(book.get_bid_with_limit(10, Coin::Inno, 1), Some(2));
        assert_eq!(book.get_bid_with_limit(10, Coin::Inno, 2), Some(2));
        assert_eq!(book.get_bid_with_limit(10, Coin::Inno, 3), Some(2));
        assert_eq!(book.get_bid_with_limit(10, Coin::Inno, 4), Some(2));

        assert_eq!(book.get_ask_with_limit(10, Coin::Inno, 0), Some(5));
        assert_eq!(book.get_ask_with_limit(10, Coin::Inno, 1), Some(5));
        assert_eq!(book.get_ask_with_limit(10, Coin::Inno, 2), Some(5));
        assert_eq!(book.get_ask_with_limit(10, Coin::Inno, 3), Some(5));
        assert_eq!(book.get_ask_with_limit(10, Coin::Inno, 4), None);
    }
}
