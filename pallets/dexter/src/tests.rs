use crate::{Error, mock::*};
use frame_support::{assert_ok, assert_noop};
use super::order::{OrderParams, Coin};
use crate::sp_api_hidden_includes_decl_storage::hidden_include::IterableStorageMap;
use crate::sp_api_hidden_includes_decl_storage::hidden_include::StorageMap;

//#[test]
fn test_two_limit_orders() {
	new_test_ext().execute_with(|| {
		let who1 = match Origin::signed(1).into() {
			Ok(frame_system::RawOrigin::Signed(t)) => Ok(t),
			_ => Err(()),
		}.unwrap();
		let who2 = match Origin::signed(2).into() {
			Ok(frame_system::RawOrigin::Signed(t)) => Ok(t),
			_ => Err(()),
		}.unwrap();
		assert_ne!(who1, who2);

		assert_ok!(DexterModule::place_ask_order(Origin::signed(1), OrderParams::LimitOrder(20, Coin::Inno)));
		assert_eq!(super::OpenAskOrders::iter().count(), 1);
		assert_eq!(super::OpenBidOrders::iter().count(), 0);
		assert_eq!(super::ClosedOrders::iter().count(), 0);
		assert_eq!(DexterModule::get_dot_balance(DexterModule::get_client_id(who1)), 80);
		assert_eq!(DexterModule::get_inno_balance(DexterModule::get_client_id(who1)), 100);
		
		assert_ok!(DexterModule::place_bid_order(Origin::signed(2), OrderParams::LimitOrder(10, Coin::Inno)));
		assert_eq!(super::OpenBidOrders::iter().count(), 0);
		assert_eq!(super::OpenAskOrders::iter().count(), 0);
		assert_eq!(super::ClosedOrders::iter().count(), 2);
		
		assert_ne!(DexterModule::get_client_id(who1), DexterModule::get_client_id(who2));
		assert_eq!(super::AccountClientId::<Test>::iter().count(), 2);
		let mut it = super::AccountClientId::<Test>::iter();
		assert_eq!(it.next().unwrap().0, who1);
		assert_eq!(it.next().unwrap().0, who2);
		assert_eq!(DexterModule::get_inno_balance(DexterModule::get_client_id(who1)), 200);
		assert_eq!(DexterModule::get_inno_balance(DexterModule::get_client_id(who2)), 0);
		assert_eq!(DexterModule::get_dot_balance(DexterModule::get_client_id(who1)), 80);
		assert_eq!(DexterModule::get_dot_balance(DexterModule::get_client_id(who2)), 120);
	});
} 

#[test]
fn test_scenario_1() {
	new_test_ext().execute_with(|| {
		assert_ok!(DexterModule::place_bid_order(Origin::signed(6), OrderParams::LimitOrder(2, Coin::Inno)));
		assert_ok!(DexterModule::place_bid_order(Origin::signed(7), OrderParams::MarketOrder(Coin::Inno)));
		assert_ok!(DexterModule::place_bid_order(Origin::signed(8), OrderParams::LimitOrder(1, Coin::Inno)));
		assert_ok!(DexterModule::place_bid_order(Origin::signed(9), OrderParams::MarketOrder(Coin::Inno)));
		assert_ok!(DexterModule::place_bid_order(Origin::signed(10), OrderParams::LimitOrder(3, Coin::Inno)));

		assert_ok!(DexterModule::place_ask_order(Origin::signed(1), OrderParams::LimitOrder(3, Coin::Inno)));
		assert_ok!(DexterModule::place_ask_order(Origin::signed(2), OrderParams::MarketOrder(Coin::Inno)));
		assert_ok!(DexterModule::place_ask_order(Origin::signed(3), OrderParams::LimitOrder(1, Coin::Inno)));
		assert_ok!(DexterModule::place_ask_order(Origin::signed(4), OrderParams::MarketOrder(Coin::Inno)));
		assert_ok!(DexterModule::place_ask_order(Origin::signed(5), OrderParams::LimitOrder(2, Coin::Inno)));
	});
}

#[test]
fn it_works_for_default_value() {
	new_test_ext().execute_with(|| {
		/*// Dispatch a signed extrinsic.
		assert_ok!(TemplateModule::do_something(Origin::signed(1), 42));
		// Read pallet storage and assert an expected result.
		assert_eq!(TemplateModule::something(), Some(42));*/
	});
}

#[test]
fn correct_error_for_none_value() {
	new_test_ext().execute_with(|| {
		// Ensure the expected error is thrown when no value is present.
		/*assert_noop!(
			TemplateModule::cause_error(Origin::signed(1)),
			Error::<Test>::NoneValue
		);*/
	});
}
