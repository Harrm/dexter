
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// 	http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

//! RPC interface for the dexter module.


use std::sync::Arc;
use sp_blockchain::HeaderBackend;
use jsonrpc_core::{Error as RpcError, ErrorCode, Result};
use jsonrpc_derive::rpc;
use sp_runtime::{generic::BlockId, traits::{Block as BlockT}};
use sp_api::ProvideRuntimeApi;

pub use pallet_dexter_runtime_api::{DexterApi as DexterRuntimeApi, Order, OrderStatus, ClosedDeal};

#[rpc]
pub trait DexterApi<BlockHash, ResponseType> {
	#[rpc(name = "dexter_getOrderStatus")]
	fn get_order_status(
		&self,
		order: Order,
	) -> Result<ResponseType>;

	#[rpc(name = "dexter_getBidOrders")]
	fn get_bid_orders(
		&self,
		limit: u32,
	) -> Result<Vec<Order>>;

	#[rpc(name = "dexter_getAskOrders")]
	fn get_ask_orders(
		&self,
		limit: u32,
	) -> Result<Vec<Order>>;

	#[rpc(name = "dexter_getClosedOrders")]
	fn get_closed_orders(
		&self,
		limit: u32,
	) -> Result<Vec<ClosedDeal>>;

}



/// A struct that implements the [`DexterApi`].
pub struct Dexter<C, P> {
	client: Arc<C>,
	_marker: std::marker::PhantomData<P>,
}

impl<C, P> Dexter<C, P> {
	/// Create new `Dexter` with the given reference to the client.
	pub fn new(client: Arc<C>) -> Self {
		Self { client, _marker: Default::default() }
	}
}

/// Error type of this RPC api.
pub enum Error {
	/// The order was not decodable.
	DecodeError,
	/// The call to runtime failed.
	RuntimeError,
}

impl From<Error> for i64 {
	fn from(e: Error) -> i64 {
		match e {
			Error::RuntimeError => 1,
			Error::DecodeError => 2,
		}
	}
}

impl<C, Block> DexterApi<
	<Block as BlockT>::Hash,
	OrderStatus,
> for Dexter<C, Block>
where
	Block: BlockT,
	C: 'static + ProvideRuntimeApi<Block> + HeaderBackend<Block>,
	C::Api: DexterRuntimeApi<Block>
{
	fn get_order_status(
		&self,
		order: Order,
	) -> Result<OrderStatus> {
		let api = self.client.runtime_api();
		let at = BlockId::hash(self.client.info().best_hash);
		
		api.get_order_status(&at, order).map_err(|e| RpcError {
			code: ErrorCode::ServerError(Error::RuntimeError.into()),
			message: "Unable to place order.".into(),
			data: Some(format!("{:?}", e).into()),
		})
	}

	fn get_bid_orders(
		&self,
		limit: u32,
	) -> Result<Vec<Order>> {
		let api = self.client.runtime_api();
		let at = BlockId::hash(self.client.info().best_hash);
		
		api.get_bid_orders(&at, limit).map_err(|e| RpcError {
			code: ErrorCode::ServerError(Error::RuntimeError.into()),
			message: "Unable to retrieve bid orders.".into(),
			data: Some(format!("{:?}", e).into()),
		})
	}

	fn get_ask_orders(
		&self,
		limit: u32,
	) -> Result<Vec<Order>> {
		let api = self.client.runtime_api();
		let at = BlockId::hash(self.client.info().best_hash);
		
		api.get_ask_orders(&at, limit).map_err(|e| RpcError {
			code: ErrorCode::ServerError(Error::RuntimeError.into()),
			message: "Unable to retrieve ask orders.".into(),
			data: Some(format!("{:?}", e).into()),
		})
	}

	fn get_closed_orders(
		&self,
		limit: u32,
	) -> Result<Vec<ClosedDeal>> {
		let api = self.client.runtime_api();
		let at = BlockId::hash(self.client.info().best_hash);
		
		api.get_closed_orders(&at, limit).map_err(|e| RpcError {
			code: ErrorCode::ServerError(Error::RuntimeError.into()),
			message: "Unable to retrieve closed orders.".into(),
			data: Some(format!("{:?}", e).into()),
		})
	}
}